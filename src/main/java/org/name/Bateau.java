package org.name;

import java.util.Set;
/*import java.util.TreeMap;*/
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="t_bateau")
public class Bateau {
	
	
	
	@Override
	public String toString() {
		return "Bateau [id=" + id + ", nom=" + nom + ", equipage=" + equipage + "]";
	}
 

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	

	@Column(name="name")
	private String nom;
	
	
	//@Column(name="equipage")
	@OneToMany(cascade=CascadeType.PERSIST,fetch=FetchType.EAGER,mappedBy="bateau")
	private Set<Marin> equipage = new TreeSet<Marin>();


	public Set<Marin> getEquipage() {
		return equipage;
	}


	public void setEquipage(Set<Marin> equipage) {
		this.equipage = equipage;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((equipage == null) ? 0 : equipage.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bateau other = (Bateau) obj;
		if (equipage == null) {
			if (other.equipage != null)
				return false;
		} else if (!equipage.equals(other.equipage))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}
	
	
	
}
